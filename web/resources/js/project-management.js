/**
 * Created by Lisa on 15/07/2016.
 */
/**
 This file is responsible for sending request to node.js to manage the project structure
 **/

var nodeSocket;
var pendingRequests = {};

function initConnectionNS() {
    nodeSocket = new WebSocket('ws://52.203.246.100:8084/');
    //nodeSocket = new WebSocket('ws://localhost:8084/');
    nodeSocket.onopen = function() {
        initIDE();
    };
    nodeSocket.onmessage = onMessageCallback;
    nodeSocket.onclose = onCloseCallback;
    nodeSocket.onerror = onErrorCallback;
}

// send getFileContent command to node
function getFileContent(fileId) {
    var requestID = getRandomrequestID();

    var getFileContentJson = {
        action: 'getFileContent',
        requestID: requestID,
        userId: getUserId(),
        projectId: getProjectId(),
        fileId: fileId
    };


    nodeSocket.send(JSON.stringify(getFileContentJson));
    pendingRequests[requestID] = 'getFileContent';
}

// send create new class command to node
function createNewClass() {
    var requestID = getRandomrequestID();
    var classNameField = $( "#new-class-form\\:new-class-name" );
    var className = classNameField.val();
    classNameField.val("");

    if (className.indexOf(".java") == -1) {
        className += ".java";
    }

    var createClassJson = {
        action: 'createClass',
        requestID: requestID,
        userId: getUserId(),
        projectId: getProjectId(),
        packageName: packageName,
        name: className
    };

    nodeSocket.send(JSON.stringify(createClassJson));
    pendingRequests[requestID] = 'createClass';
}

// send delete class command to node
function deleteClass() {
    var requestID = getRandomrequestID();

    var className = null; // TODO: retrieve class name

    var deleteClassJson = {
        action: 'deleteClass',
        requestID: requestID,
        userId: getUserId(),
        name: className
    };

    nodeSocket.send(JSON.stringify(deleteClassJson));
    pendingRequests[requestID] = 'deleteClass';
}

// send create new package command to node
function createNewPackage() {
    var requestID = getRandomrequestID();

    var packageNameField = $( "#new-package-form\\:new-package-name" );
    var name = packageNameField.val();
    packageNameField.val("");

    var createClassJson = {
        action: 'createPackage',
        requestID: requestID,
        userId: getUserId(),
        projectId: getProjectId(),
        packageName: packageName,
        name: name
    };

    nodeSocket.send(JSON.stringify(createClassJson));
    pendingRequests[requestID] = 'createClass';
}

var onMessageCallback = function(event) {
    var jsonResponse = JSON.parse(event.data);

    if (jsonResponse.responseCode != 200) {
        displayErrorMessage(jsonResponse.responseComment);
        delete pendingRequests[jsonResponse.requestID];
        return;
    }

    switch (pendingRequests[jsonResponse.requestID]) {
        case 'getFileContent':
            handleResponse.getFileContent(jsonResponse);
            break;
        case 'createClass':
            handleResponse.createClass(jsonResponse);
            break;
        case 'deleteClass':
            handleResponse.deleteClass(jsonResponse);
            break;
        case 'createPackage':
            handleResponse.createPackage(jsonResponse);
            break;
        default:
            break;
    }

    delete pendingRequests[jsonResponse.requestID];
};

var onErrorCallback = function(event) {
    console.log('Error: ' + JSON.stringify(event));
    nodeSocket = undefined;
};

var onCloseCallback = function(event) {
    console.log('Closed connection with Node: ' + event.data);
    nodeSocket = undefined;
};

var handleResponse = {

    getFileContent: function(response) {
        var fileId = response.fileId;
        // Store the content of the file
        openedProjectFiles[fileId] = response.content;
        // Create a new ot.Client, override methods, and keep track of the revision number
        var newClient = new ot.Client(fileId, response.revision);
        newClient.applyOperation = applyOperationOverride;
        newClient.sendOperation = sendOperationOverride;

        fileClients[fileId] = newClient;
        if (fileId == getActiveFileId()) {
            setFileContentInACEEditor(fileId);
        }
    },

    /**
     * Refresh project structure on response successful and display new file in editor,
     * display error message otherwise
     * @param response in the form
     *      requestID: "",
     *      responseCode: 200 or 400,
     *      responseComment: confirmation/error message
     */
    createClass: function(response) {
        $( '#refresh-project-structure-button' ).click();
        
        // Wait for AJAX response for refreshing project structure then display file
        filePendingToBeDisplayed = {
            id: response.fileId,
            path: response.responseComment
        };
    },

    /**
     * Refresh project list on response successful, display error message otherwise
     * @param response in the form
     *      requestID: "",
     *      responseCode: 200 or 400,
     *      responseComment: confirmation/error message
     */
    deleteClass: function(response) {
        // TODO
    },

    /**
     * Refresh project structure on response successful, display error message otherwise
     * @param response in the form
     *      requestID: "",
     *      responseCode: 200 or 400,
     *      responseComment: confirmation/error message
     */
    createPackage: function(response) {
        $( '#refresh-project-structure-button' ).click();
    }
};

function displayErrorMessage(msg) {
    //TODO
}

function getRandomrequestID() {
    return Math.random().toString(36).substring(2);
}