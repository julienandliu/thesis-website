/**
 * This file is responsible for implementing the real-time synchronisation between simultaneously
 * connected users. A connection is open on port 8085 of the Node.js app and all modifications
 * made by the users are immediately sent.
 */
var realTimeSocket;

// Keep track of compilation errors for each files
var compilationErrors = {};

// Collaborators details are hidden by JSF and loaded into this map on document.ready
var collaboratorsInfo = {};

// Keep track of each collaborators active file (can be undefinded, even if a user is connected to a project)
var filesActiveCollaborators = {};

// Map fileIds to the corresponding ot.Client
var fileClients = {};

function initConnectionRTS() {
    realTimeSocket = new WebSocket('ws://52.203.246.100:8085/');
    //realTimeSocket = new WebSocket('ws://localhost:8085/');
    realTimeSocket.onopen = onOpenCallbackRTS;
    realTimeSocket.onmessage = onMessageCallbackRTS;
    realTimeSocket.onclose = onCloseCallbackRTS;
    realTimeSocket.onerror = onErrorCallbackRTS;
}

var onOpenCallbackRTS = function() {
    var requestID = getRandomrequestID();

    var initConnectionJson = {
        action: 'initConnection',
        requestID: requestID,
        userId: getUserId(),
        projectId: getProjectId()
    };

    realTimeSocket.send(JSON.stringify(initConnectionJson));

    initConnectionNS();
};

var onMessageCallbackRTS = function(event) {
    var jsonResponse = JSON.parse(event.data);

    switch (jsonResponse.action) {
        case 'initConnection':
            handleResponseRTS.initConnection(jsonResponse);
            break;
        case 'operation':
            handleResponseRTS.operationReceived(jsonResponse);
            break;
        case 'notification':
            handleResponseRTS.handleNotification(jsonResponse);
            break;
        case 'compilation':
            handleResponseRTS.compilation(jsonResponse);
            break;
        default:
            break;
    }
};

var onErrorCallbackRTS = function(event) {
    console.log('Error: ' + JSON.stringify(event));
    realTimeSocket = undefined;
};

var onCloseCallbackRTS = function(event) {
    console.log('Closed connection with Node: ' + event.data);
    realTimeSocket = undefined;
};

var handleResponseRTS = {

    /**
     * Display collaborators connected
     *
     * @param msgJson in the form
     *      action: "",
     *      collaboratorsConnected: [],
     *      error: "" (IF ANY)
     */
    initConnection: function(msgJson) {
        console.log('initConnection response: ' + JSON.stringify(msgJson));
        updateCollaboratorsConnected(msgJson.collaboratorsConnected);

        msgJson.collaboratorsActiveFile.forEach(function(item, element) {
            updateCollaboratorsActiveFile(item.userId, item.fileId);
        });

        if (msgJson.collaboratorsActiveFile.length != 0) {
            displayCollaboratorsActiveFile();
        }
    },

    /**
     * Called when an operation is received, update the corresponding client which will trigger
     * another function to update the editor.
     *
     * @param msgJson in the form
     *      action: "",
     *      fileId: int
     *      operation: ot.Operation
     */
    operationReceived: function(msgJson) {
        var operation = ot.TextOperation.fromJSON(msgJson.operation);
        console.log("received operation: " + JSON.stringify(msgJson.operation) + " from user: " + msgJson.userId);
        if (msgJson.userId == getUserId()) {
            fileClients[msgJson.fileId].serverAck();
        } else {
            fileClients[msgJson.fileId].applyServer(operation);
        }
    },

    handleNotification: function(msgJson) {
        switch (msgJson.type) {
            case 'projectStructureUpdate':
                $( '#refresh-project-structure-button' ).click();
                break;
            case 'collaboratorsConnected':
                updateCollaboratorsConnected(msgJson.collaboratorsConnected);
                break;
            case 'activeFileSet':
                updateCollaboratorsActiveFile(msgJson.userId, msgJson.fileId);
                displayCollaboratorsActiveFile();
                break;
            default:
                break;
        }
    },

    /**
     * When receiving a new set of compilation errors, the previous ones are cleared,
     * and the new ones are added. That way if the new set of compilation errors has no
     * errors on the currently active file, the annotations disappear.
     * @param msgJson
     */
    compilation: function(msgJson) {
        compilationErrors = msgJson.compilationErrors;
        setCompilationErrors();
    }
};

/**
 * Method called when an operation is received from the server (fired after calling applyServer).
 * Update the file content stored in openedProjectFiles.
 * If the file is currently open in the editor, insert the change.
 *
 * @param fileId
 * @param operation
 */
var applyOperationOverride = function(fileId, operation) {

    if (fileId == getActiveFileId()) {
        handleLocalChanges.insertChangesIntoEditor(operation);
    }

    // Update the document
    var doc = openedProjectFiles[fileId];
    openedProjectFiles[fileId] = operation.apply(doc);
};

/**
 * Triggered by onUserChange -> ot.Client.applyClient.
 * Send the operation to the server.
 * 
 * @param revision
 * @param operation
 */
var sendOperationOverride = function (revision, operation) {
    var op = {
        action: 'operation',
        userId: getUserId(),
        projectId: getProjectId(),
        fileId: getActiveFileId(),
        revision: revision,
        operation: operation
    };

    console.log('Sending operation: ' + JSON.stringify(operation));
    realTimeSocket.send(JSON.stringify(op));
};


/**
 * Stores collaborators details in a collaboratorsInfo. In the form
 * userId : {
 *              firstName: ""
 *              lastName: ""
 *              projectStructureDisplay: ""
 *              tabDisplay: ""
 *              activeFile: int
 *          },
 * userId : {
 *              ....
 */
function initCollaboratorsInfo() {

    var collaboratorsHTML = $( "#project-collaborators" ).children( ".collaborator-icon" );

    collaboratorsHTML.each(function() {

        var id = Number($(this).children( ".hidden-collaborator-id" ).text());
        var firstName = $(this).children( ".hidden-collaborator-firstName" ).text();
        var lastName = $(this).children( ".hidden-collaborator-lastName" ).text();
        var projectStructureDisplay = firstName + " " + lastName.substring(0, 1) + ".";
        var tabDisplay = firstName.substring(0, 1) + lastName.substring(0, 1);

        collaboratorsInfo[id] = {
            firstName: firstName,
            lastName: lastName,
            projectStructureDisplay: projectStructureDisplay,
            tabDisplay: tabDisplay
        };
    });
}

function updateCollaboratorsConnected(collaboratorsConnected) {

    var collaboratorsElement = $( "#project-collaborators" ).children( ".collaborator-icon" );

    collaboratorsElement.each(function() {

        var collaboratorId = Number($(this).children( ".hidden-collaborator-id" ).text());

        if (collaboratorsConnected.indexOf(collaboratorId) != -1) {
            $(this).css({opacity:'1'})
        } else {
            $(this).css({opacity:'0.25'})
        }
    });
}

function updateCollaboratorsActiveFile(userId, fileId) {

    console.log("collaboratorsInfo: " + JSON.stringify(collaboratorsInfo));
    // Remove userId from filesActiveCollaborators
    var previousActiveFile = collaboratorsInfo[userId].activeFile;
    if (previousActiveFile != undefined) {
        var index = filesActiveCollaborators[previousActiveFile].indexOf(userId);
        filesActiveCollaborators[previousActiveFile].splice(index, 1);
    }

    // Replace active file in collaboratorsInfo
    collaboratorsInfo[userId].activeFile = fileId;

    // Add userId to filesActiveCollaborators
    if (filesActiveCollaborators[fileId] == undefined) {
        filesActiveCollaborators[fileId] = [userId];
    } else {
        filesActiveCollaborators[fileId].push(userId);
    }
}

function displayCollaboratorsActiveFile() {
    displayCollaboratorActiveFileInTabBar();
    displayCollaboratorActiveFileInProjectStructure();
}

function displayCollaboratorActiveFileInTabBar() {
    var openTabsHTML =  $( "#tab-bar" ).children( "a" );

    openTabsHTML.each(function() {

        var tab = $(this).children( ".tab" );

        // Remove all previous user marker
        var tabMarker = tab.children( ".collaborator-tab-marker" );
        tabMarker.each(function() {

            $( $(this), tab ).remove();

        });

        // Add collaborators markers to tabs
        var fileId = Number($(this).children( ".hidden-element-id" ).text());

        var fileCollaborators = filesActiveCollaborators[fileId];
        if (fileCollaborators != undefined) {

            fileCollaborators.forEach(function(item, index) {

                var tabDisplay = collaboratorsInfo[item].tabDisplay;
                var markerHTML = "<div class='collaborator-tab-marker'><p>" + tabDisplay + "</p></div>";
                tab.children( "p" ).after(markerHTML);
            });

        }
        
    });
}

function displayCollaboratorActiveFileInProjectStructure() {
    var rootChildren = $( "#project-outline" ).children( "ul.children" );
    updateMarkersInProjectStructure(rootChildren);
}

/**
 * Iterate through the DOM elements of the project outline following a DFS algorithm.
 * For each files encountered, remove ols markers then add the current ones.
 */
function updateMarkersInProjectStructure(arrayElement) {

    var files = arrayElement.children( "a" );
    files.each(function() {

        var fileLine = $(this).children( "li.file" ).children( ".file-line" );

        // Remove all previous user markers
        var tabMarker = fileLine.children( ".collaborator-file-marker" );
        tabMarker.each(function() {

            $( $(this), fileLine ).remove();

        });

        // Add collaborators markers to files line
        var fileId = Number($(this).children( "li.file" ).children( ".hidden-element-id" ).text());

        var fileCollaborators = filesActiveCollaborators[fileId];
        if (fileCollaborators != undefined) {

            fileCollaborators.forEach(function(item, index) {

                var projectStructureDisplay = collaboratorsInfo[item].projectStructureDisplay;
                fileLine.append(
                    "<div class='collaborator-file-marker'><p>" + projectStructureDisplay + "</p></div>"
                );

            });

        }

    });

    var folders = arrayElement.children( "li.folder" );
    folders.each(function() {

        var children = $(this).children( "ul.children" );
        updateMarkersInProjectStructure(children);

    });
}

function setCompilationErrors() {

    // Add error markers in project outline
    addErrorMarkersInProjectOutline();

    // This method can be called - receiving collaborators update - before the user opens a file and starts the editor
    if (editor == null) { return; }

    // Add error markers in tabs
    addErrorMarkersInTabs();

    // In ACE editor - Clear previous annotations and add new ones
    addErrorAnnotationsInEditor();
}

function addErrorMarkersInProjectOutline() {

    var allErrors = getAllCompilationErrors();

    var elementToMarkAtIndentationLevel = {};

    allErrors.forEach(function(item, index) {
        var path = item.fileID;
        var pathArray = path.split('/');

        for (var indent = 0; indent < pathArray.length; indent++) {
            var element = pathArray[indent];

            if (elementToMarkAtIndentationLevel[indent] == undefined) {
                elementToMarkAtIndentationLevel[indent] = [element];
            } else if (elementToMarkAtIndentationLevel[indent].indexOf(element) == -1) {
                elementToMarkAtIndentationLevel[indent].push(element);
            } else {
                // Do nothing
            }
        }

    });

    console.log("ElementToMarkAtIndentationLevel:" + JSON.stringify(elementToMarkAtIndentationLevel));

    var rootChildren = $( "#project-outline" ).children( "ul.children" );
    updateErrorMarkersInProjectOutline(rootChildren, 0, elementToMarkAtIndentationLevel);
}

function updateErrorMarkersInProjectOutline(arrayElement, indentationLevel, elementToMarkAtIndentationLevel) {

    var files = arrayElement.children( "a" );
    files.each(function() {

        var fileLine = $(this).children( "li.file" ).children( ".file-line" );
        var fileName = fileLine.children( "p" ).text();

        var fileNeedsToBeMarked;
        if (elementToMarkAtIndentationLevel[indentationLevel] == undefined) {
            fileNeedsToBeMarked = false;
        } else {
            fileNeedsToBeMarked = elementToMarkAtIndentationLevel[indentationLevel].indexOf(fileName) != -1;
        }

        var crossMarker = fileLine.children( ".compilation-cross-marker" );
        var fileIsMarked = crossMarker.length > 0;

        if (fileNeedsToBeMarked && !fileIsMarked) {

            fileLine.append(
                "<img class='compilation-cross-marker' "
                + "src='../../resources/img/compilation-cross.png' alt=''/>"
            );

        } else if (!fileNeedsToBeMarked && fileIsMarked) {

            crossMarker.each(function() {
                $( $(this), fileLine ).remove();
            });

        }
    });

    var folders = arrayElement.children( "li.folder" );

    folders.each(function() {

        var folderLine = $(this).children(".folder-line");
        var folderName = folderLine.children("p").text();

        var folderNeedsToBeMarked;
        if (elementToMarkAtIndentationLevel[indentationLevel] == undefined) {
            folderNeedsToBeMarked = false;
        } else {
            folderNeedsToBeMarked =
                elementToMarkAtIndentationLevel[indentationLevel].indexOf(folderName) != -1;
        }

        var crossMarkerFolder = folderLine.children( ".compilation-cross-marker" );
        var folderIsMarked = crossMarkerFolder.length > 0;

        if (folderNeedsToBeMarked && !folderIsMarked) {

            folderLine.append(
                "<img class='compilation-cross-marker' "
                + "src='../../resources/img/compilation-cross.png' alt=''/>"
            );

        } else if (!folderNeedsToBeMarked && folderIsMarked) {

            crossMarkerFolder.each(function() {
                $( $(this), folderLine ).remove();
            });

        }

        var children = $(this).children( "ul.children" );
        updateErrorMarkersInProjectOutline(children, indentationLevel + 1, elementToMarkAtIndentationLevel);
    });

}

function addErrorMarkersInTabs() {

    var allErrors = getAllCompilationErrors();

    var fileToMarkInTabs = [];

    allErrors.forEach(function(item, index) {
        var path = item.fileID;
        var pathArray = path.split('/');

        for (var indent = 0; indent < pathArray.length; indent++) {
            var element = pathArray[indent];

            if (indent == pathArray.length - 1 && fileToMarkInTabs.indexOf(element) == -1) {
                fileToMarkInTabs.push(element);
            }
        }

    });

    var openTabsHTML =  $( "#tab-bar" ).children( "a" );

    openTabsHTML.each(function() {

        var tab = $(this).children( ".tab" );
        var fileName = tab.children("p").text();

        var tabNeedsToBeMarked = fileToMarkInTabs.indexOf(fileName) != -1;
        var crossMarker = tab.children( ".compilation-cross-marker" );
        var tabIsMarked = crossMarker.length > 0;


        if (tabNeedsToBeMarked && !tabIsMarked) {

            tab.append(
                "<img class='compilation-cross-marker' "
                + "src='../../resources/img/compilation-cross.png' alt=''/>"
            );

        } else if (!tabNeedsToBeMarked && tabIsMarked) {

            crossMarker.each(function() {
                $( $(this), tab ).remove();
            });

        }

    });
}

function addErrorAnnotationsInEditor() {

    // Clear annotations before adding new ones
    editor.session.clearAnnotations();

    var errors = compilationErrors[getActiveFileId()];
    if (errors == undefined) { return; }

    // Add error markers in editor
    editor.session.setAnnotations(errors);
}

function getAllCompilationErrors() {
    var allErrors = [];
    var keys = Object.keys(compilationErrors);
    keys.forEach(function(fileId, index) {
        var fileErrors = compilationErrors[fileId];
        fileErrors.forEach(function(error, index) {
            if (error.type == "error") {
                allErrors.push(error);
            }
        });
    });
    return allErrors;
}