/**
 * Created by Lisa on 15/07/2016.
 */
/**
 This file is responsible for sending create/modify/delete project request to node.js
 **/

var nodeSocket;
var pendingRequests = {};

function initConnection() {

    nodeSocket = new WebSocket('ws://52.203.246.100:8083/');
    //nodeSocket = new WebSocket('ws://localhost:8083/');
    nodeSocket.onopen = function() {
        console.log("Connection Opened");
    };
    nodeSocket.onmessage = onMessageCallback;
    nodeSocket.onclose = onCloseCallback;
    nodeSocket.onerror = onErrorCallback;
}

// send create new project command to node
function createNewProject() {
    var requestID = getRandomrequestID();
    var userId = getUserId();

    var projectNameField = $( "#new-project-form\\:new-project-name" );
    var projectName = projectNameField.val();
    projectNameField.val("");

    var createProjectJson = {
        action: 'createProject',
        requestID: requestID,
        userId: userId,
        name: projectName
    };

    nodeSocket.send(JSON.stringify(createProjectJson));
    pendingRequests[requestID] = 'createProject';
}

// send delete project command to node
function deleteProject(deleteButton) {
    var requestID = getRandomrequestID();
    var projectName = null; // TODO: retrieve project name

    var deleteProjectJson = {
        action: 'deleteProject',
        requestID: requestID,
        name: projectName
    };

    nodeSocket.send(JSON.stringify(deleteProjectJson));
    pendingRequests[requestID] = 'deleteProject';
}

var onMessageCallback = function(event) {
    var jsonResponse = JSON.parse(event.data);

    if (jsonResponse.responseCode != 200) {
        displayErrorMessage(jsonResponse.responseComment);
        delete pendingRequests[jsonResponse.requestID];
        return;
    }
    
    switch (pendingRequests[jsonResponse.requestID]) {
        case 'createProject':
            handleResponse.createProject(jsonResponse);
            break;
        case 'deleteProject':
            handleResponse.deleteProject(jsonResponse);
            break;
        default:
            break;
    }

    delete pendingRequests[jsonResponse.requestID];
};

var onErrorCallback = function(event) {
    console.log('Error: ' + JSON.stringify(event));
    nodeSocket = undefined;
};

var onCloseCallback = function(event) {
    console.log('Closed connection with Node: ' + event.data);
    nodeSocket = undefined;
};

var handleResponse = {

    /**
     * Refresh project list on response successful, display error message otherwise
     * @param response in the form
     *      requestID: "",
     *      responseCode: 200 or 400,
     *      responseComment: confirmation/error message
     */
    createProject: function(response) {
        $( '#refresh-project-list-button' ).click();
    },

    /**
     * Refresh project list on response successful, display error message otherwise
     * @param response in the form
     *      requestID: "",
     *      responseCode: 200 or 400,
     *      responseComment: confirmation/error message
     */
    deleteProject: function(response) {
        // TODO
    }
};

function displayErrorMessage(msg) {
    //TODO
}

function getRandomrequestID() {
    return Math.random().toString(36).substring(2);
}