var emailPlaceholder = "E-mail";
var passwordPlaceholder = "Password";
var firstNamePlaceholder = "First Name";
var lastNamePlaceholder = "Last Name";

$(document).ready(function() {

    $( '#reset-project-selected' ).click();


    var emailField = $( "#login-form\\:email-login-form" );
    emailField.val(emailPlaceholder);
    emailField.addClass( "placeholder" );
    emailField.focus( function() {
        if (emailField.val() == emailPlaceholder) {
            emailField.val("");
            emailField.removeClass( "placeholder" );
        }
    });
    emailField.blur( function() {
        if (emailField.val() == "") {
            emailField.val(emailPlaceholder);
            emailField.addClass( "placeholder" );
        }
    });

    var passwordField = $( "#login-form\\:password-login-form" );
    passwordField.val(passwordPlaceholder);
    passwordField.addClass( "placeholder" );
    passwordField.focus( function() {
        if (passwordField.val() == passwordPlaceholder) {
            passwordField.val("");
            passwordField.removeClass( "placeholder" );
        }
    });
    passwordField.blur( function() {
        if (passwordField.val() == "") {
            passwordField.val(passwordPlaceholder);
            passwordField.addClass( "placeholder" );
        }
    });

    var firstNameField = $( "#signup-form\\:first-name-signup-form" );
    firstNameField.val(firstNamePlaceholder);
    firstNameField.addClass( "placeholder" );
    firstNameField.focus( function() {
        if (firstNameField.val() == firstNamePlaceholder) {
            firstNameField.val("");
            firstNameField.removeClass( "placeholder" );
        }
    });
    firstNameField.blur( function() {
        if (firstNameField.val() == "") {
            firstNameField.val(firstNamePlaceholder);
            firstNameField.addClass( "placeholder" );
        }
    });

    var lastNameField = $( "#signup-form\\:last-name-signup-form" );
    lastNameField.val(lastNamePlaceholder);
    lastNameField.addClass( "placeholder" );
    lastNameField.focus( function() {
        if (lastNameField.val() == lastNamePlaceholder) {
            lastNameField.val("");
            lastNameField.removeClass( "placeholder" );
        }
    });
    lastNameField.blur( function() {
        if (lastNameField.val() == "") {
            lastNameField.val(lastNamePlaceholder);
            lastNameField.addClass( "placeholder" );
        }
    });

    var emailSignupField = $( "#signup-form\\:email-signup-form" );
    emailSignupField.val(emailPlaceholder);
    emailSignupField.addClass( "placeholder" );
    emailSignupField.focus( function() {
        if (emailSignupField.val() == emailPlaceholder) {
            emailSignupField.val("");
            emailSignupField.removeClass( "placeholder" );
        }
    });
    emailSignupField.blur( function() {
        if (emailSignupField.val() == "") {
            emailSignupField.val(emailPlaceholder);
            emailSignupField.addClass( "placeholder" );
        }
    });

    var passwordSignupField = $( "#signup-form\\:password-signup-form" );
    passwordSignupField.val(passwordPlaceholder);
    passwordSignupField.addClass( "placeholder" );
    passwordSignupField.focus( function() {
        if (passwordSignupField.val() == passwordPlaceholder) {
            passwordSignupField.val("");
            passwordSignupField.removeClass( "placeholder" );
        }
    });
    passwordSignupField.blur( function() {
        if (passwordSignupField.val() == "") {
            passwordSignupField.val(passwordPlaceholder);
            passwordSignupField.addClass( "placeholder" );
        }
    });

});

function validateLoginInput() {
    var isValid = true;
    var emailField = $( "#login-form\\:email-login-form" );
    var passwordField = $( "#login-form\\:password-login-form" );
    
    if (emailField.val() == emailPlaceholder) {
        emailField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        emailField.css({borderColor: 'lightgrey'});
    }

    if (passwordField.val() == passwordPlaceholder) {
        passwordField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        passwordField.css({borderColor: 'lightgrey'});
    }

    return isValid;
}

function validateSignUpForm() {
    var isValid = true;
    var emailSignupField = $( "#signup-form\\:email-signup-form" );
    var passwordSignupField = $( "#signup-form\\:password-signup-form" );
    var firstNameField = $( "#signup-form\\:first-name-signup-form"  );
    var lastNameField = $( "#signup-form\\:last-name-signup-form"  );

    if (emailSignupField.val() == emailPlaceholder) {
        emailSignupField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        emailSignupField.css({borderColor: 'lightgrey'});
    }

    if (passwordSignupField.val() == passwordPlaceholder) {
        passwordSignupField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        passwordSignupField.css({borderColor: 'lightgrey'});
    }


    if (firstNameField.val() == firstNamePlaceholder) {
        firstNameField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        firstNameField.css({borderColor: 'lightgrey'});
    }

    if (lastNameField.val() == lastNamePlaceholder) {
        lastNameField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        lastNameField.css({borderColor: 'lightgrey'});
    }

    return isValid;
}
