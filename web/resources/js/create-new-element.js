var path;
var packageName;

var inSrc;
var contextMenuIdDisplayed;

function savePath(htmlElement) {
    var selected = $( htmlElement );
    var type = selected.children( ".hidden-element-type" ).text();

    packageName = selected.children( ".hidden-element-package" ).text();
    inSrc = packageName != 'null';

    path = getProjectName() + selected.children( ".hidden-element-path" ).text();
    if (type != 'package' && type != 'folder') {
        var pathArray = path.split('/');
        pathArray.splice(pathArray.length - 1, 1);
        path = "";
        pathArray.forEach(function(element, index) {
            path += element + '/';
        });
    } else {
        path += '/';
    }
}

function showContextMenu(event) {
    if (inSrc) {
        contextMenuIdDisplayed = "#project-outline-context-menu-in-src";
    } else {
        contextMenuIdDisplayed = "#project-outline-context-menu";
    }

    var menu = $( contextMenuIdDisplayed );

    menu.css({top:event.clientY - 10, left:event.clientX - 10});
    menu.fadeToggle(300);

    event.stopPropagation();

    menu.on( "mouseleave", function() {
        menu.off("mouseleave");
        menu.fadeToggle(300);
    });

    return false;
}

function hideContextMenu() {
    var menu = $( contextMenuIdDisplayed );
    menu.off("mouseleave");
    menu.fadeToggle(300);
}

function highlightElement(htmlElement) {
    $( htmlElement ).css({backgroundColor:'#D89570'});
}

function removeHighlight(htmlElement) {
    $( htmlElement ).css({backgroundColor:'white'});
}

function showNewElementModal(elementType) {
    var top, left;
    var id = "#new-" + elementType + "-block-view";

    $( "#new-" + elementType + "-block-view" ).css("display", "initial");
    $modal = $("#new-" + elementType + "-form");

    top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
    left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

    $modal.css({top:top, left:left});

    $( "#new-" + elementType + "-form\\:new-" + elementType + "-path" ).text(path);
}

function hideNewElementModal(elementType) {
    var id = "#new-" + elementType + "-block-view";
    $( id ).css("display", "none");
}

function validateNewElement(elementType) {
    var isValid = true;

    var elementNameField = $( "#new-" + elementType + "-form\\:new-" + elementType + "-name" );

    if (elementNameField.val() == "") {
        elementNameField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        elementNameField.css({borderColor: 'lightgrey'});
    }

    return isValid;
}