/**
 * Created by romainjulien on 9/05/2016.
 */
var collaboratorsShown = false;
var snapshotsShown = false;

var userId;

var collaboratorPlaceholder = "Share project with ...";

$(document).ready(function() {
    userId = $( "#hidden-user-id").text();

    initConnection();
});

// Reset menu selected when leaving the page
// Having the menu manager to be ViewScoped causes issues with the commandlinks
window.addEventListener('beforeunload', function() {
    $( '#reset-menu-selected' ).click();
});

function getUserId() {
    return userId;
}

function handleRenderMenuChanges(data) {
    if (data.status != "success") {
        return;
    }

    var menuSelected;

    $( ".menu-item" ).each(function() {
        var item = $(this).children( "a" );
        if (item.attr('class').indexOf('menu-selected') != -1) {
            menuSelected = item.text();
        }
    });

    if (menuSelected == 'My Projects') {
        $( "#shared-project-list" ).css("display", "none");
        $( "#my-project-list" ).css("display", "block");
    } else if (menuSelected == 'Shared Projects') {
        $( "#my-project-list").css("display", "none");
        $( "#shared-project-list" ).css("display", "block");
    }
}


function showNewProjectModal() {
    var top, left;
    $("#new-project-block-view").css("display", "initial");
    $modal = $("#new-project-form");

    top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
    left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

    $modal.css({top:top, left:left});
}

function hideNewProjectModal() {
    $("#new-project-block-view").css("display", "none");
}

function toggleCollaboratorsDropDown() {
    if (!collaboratorsShown) {
        $("#invisible-overlay").show();

        setCollaboratorPlaceholder();
        collaboratorsShown = true;
    }

    $(".collaborators-drop-down").fadeToggle(300);
}

function setCollaboratorPlaceholder() {
    var collaboratorField = $( "#add-collaborators\\:new-collaborator" );

    collaboratorField.val(collaboratorPlaceholder);
    collaboratorField.addClass( "placeholder" );
    collaboratorField.css({borderColor: 'lightgrey'});

    $( "#error-collaborator" ).text("");

    collaboratorField.focus( function() {
        if (collaboratorField.val() == collaboratorPlaceholder) {
            collaboratorField.val("");
            collaboratorField.removeClass( "placeholder" );
        }
    });
    collaboratorField.blur( function() {
        if (collaboratorField.val() == "") {
            collaboratorField.val(collaboratorPlaceholder);
            collaboratorField.addClass( "placeholder" );
        }
    });
}

function toggleSnapshotsDropDown() {
    if (!snapshotsShown) {
        $("#invisible-overlay").show();
        snapshotsShown = true;
    }
    $(".snapshots-drop-down").fadeToggle(300);
}

function hideAllDropDown() {
    $("#invisible-overlay").hide();
    if (collaboratorsShown) {
        toggleCollaboratorsDropDown();
        collaboratorsShown = false;
    } else if (snapshotsShown) {
        toggleSnapshotsDropDown();
        snapshotsShown = false;
    }
}


function validateNewProject() {
    var isValid = true;

    var projectNameField = $( "#new-project-form\\:new-project-name" );

    if (projectNameField.val() == "") {
        projectNameField.css({borderColor: '#d85a30'});
        isValid = false;
    } else {
        projectNameField.css({borderColor: 'lightgrey'});
    }

    return isValid;
}

function validateNewCollaborator() {
    var input = $( "#add-collaborators\\:new-collaborator" );
    input.blur();

    if (input.val() == collaboratorPlaceholder) {
        input.css({borderColor: '#d85a30'});
        return false;
    }
    input.css({borderColor: 'lightgrey'});

    return true;
}
