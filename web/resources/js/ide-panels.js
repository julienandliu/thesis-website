var projectSettingsIsShown = false;
var chatPanelIsShown = false;
var fileRevisionPanelIsShown = false;

function toggleChatPanel() {
    if (fileRevisionPanelIsShown) {
        $("#file-revision-panel-wrapper").css({display: 'none'});
        $("#file-revision-side-menu-item").css({backgroundColor: '#f1f1f1'});
        $("#file-revision-panel").animate({width: '0', marginLeft: '0', borderWidth: '0'}, "slow");
        fileRevisionPanelIsShown = false;
    }

    if (chatPanelIsShown) {
        $("#chat-panel-wrapper").css({display: 'none'});
        $('#chat-side-menu-item').css({backgroundColor: '#f1f1f1'});
        $("#chat-panel").animate({width: '0', marginLeft: '0', borderWidth: '0'}, "slow");
    } else {
        $('#chat-side-menu-item').css({backgroundColor: 'white'});
        $("#chat-panel").animate({width: '250px', marginLeft: '-251px', borderWidth: '1px'}, "slow", function() {
            $("#chat-panel-wrapper").css({display: 'block'});
        });
    }
    chatPanelIsShown = !chatPanelIsShown;
}

function toggleFileRevisionPanel() {
    if (chatPanelIsShown) {
        $("#chat-panel-wrapper").css({display: 'none'});
        $('#chat-side-menu-item').css({backgroundColor: '#f1f1f1'});
        $("#chat-panel").animate({width: '0', marginLeft: '0', borderWidth: '0'}, "slow");
        chatPanelIsShown = false;
    }

    if (fileRevisionPanelIsShown) {
        $("#file-revision-panel-wrapper").css({display: 'none'});
        $("#file-revision-side-menu-item").css({backgroundColor: '#f1f1f1'});
        $("#file-revision-panel").animate({width: '0', marginLeft: '0', borderWidth: '0'}, "slow");
    } else {
        $("#file-revision-side-menu-item").css({backgroundColor: 'white'});
        $("#file-revision-panel").animate({width: '250px', marginLeft: '-251px', borderWidth: '1px'}, "slow", function() {
            $("#file-revision-panel-wrapper").css({display: 'block'});
        });
    }
    fileRevisionPanelIsShown = !fileRevisionPanelIsShown;
}

function toggleProjectSettings() {
    if (projectSettingsIsShown) {
        $("#project-settings-wrapper").css({display: 'none'});

        $("#project-settings").animate({height: '0'}, "slow");
        $("#main-content-project").animate({height: '90%'}, "slow");

        $("#chat-panel").animate({height: '89.2%'}, "slow");
        $("#file-revision-panel").animate({height: '89.2%'}, "slow");

        $("#tab-bar > .tab > p").animate({fontSize: '12px'}, "slow");
        $("#tab-bar > .tab").animate({padding: '3px 5px'}, "slow");
    } else {
        $("#project-settings").animate({height: '25%'}, "slow", function() {
            $("#project-settings-wrapper").css({display: 'block'});
        });
        $("#main-content-project").animate({height: '65%'}, "slow");

        $("#chat-panel").animate({height: '64.2%'}, "slow");
        $("#file-revision-panel").animate({height: '64.2%'}, "slow");

        $("#tab-bar > .tab > p").animate({fontSize: '10px'}, "slow");
        $("#tab-bar > .tab").animate({padding: '2px 5px'}, "slow");
    }
    projectSettingsIsShown = !projectSettingsIsShown;
}