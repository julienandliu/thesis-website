var editor = null;
var AceRange = ace.require('ace/range').Range;


var userId;
var projectId;
var projectName;

var activeFileId = null;
var openedProjectFiles = {};

var filePendingToBeDisplayed = null;
var tabClosed = null;

/**
 * We do not init the ACE editor right away, in case no files are open when the project
 * is opened (eg just after creating the project)
 */
$(document).ready(function() {
    
    // Get variables hidden by JSF
    userId = Number($( "#hidden-user-id" ).text());
    projectId = Number($( "#hidden-project-id" ).text());
    projectName = $( "#hidden-project-name" ).text();

    // Project collaborators details are hidden by jsf, retrieve them and store in dictionary
    initCollaboratorsInfo();

    // Send message to node to register to the connection pool and get the collaborators connected
    initConnectionRTS();
});

/**
 * This function can only be called after both the realTimeSocket and the nodeSocket are opened.
 */
function initIDE() {
    var openTabsHTML =  $( "#tab-bar" ).children( "a" );

    openTabsHTML.each(function() {
        var id = Number($(this).children( '.hidden-element-id' ).text());
        var tab = $(this).children( '.tab' );

        var activeFileId = null;
        if (tab.attr('class').indexOf('selected') != -1) {
            activeFileId = id;
        }

        // Need to get file content before setting active file
        getFileContent(id);

        if (activeFileId != null) {
            setActiveFileId(activeFileId);
        }
    });
}



function initEditor() {
    ace.require("ace/ext/language_tools");
    editor = ace.edit("ide-editor");
    editor.setTheme("ace/theme/eclipse");
    editor.getSession().setMode("ace/mode/java");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
    });

    editor.on("change", handleLocalChanges.onUserChangeAce);
}

// Send message to node before connection closes, and save last connection state
window.addEventListener('beforeunload', function() {

    var closeConnection = {
        action: 'closeConnection',
        userId: getUserId(),
        projectId: getProjectId(),
        openedFilesId: getOpenedProjectFiles()
    };
    realTimeSocket.send(JSON.stringify(closeConnection));

    // Save last connection state
    $( '#save-last-connection-state' ).click();
});

function getUserId() {
    return userId;
}

function getProjectId() {
    return projectId;
}

function getProjectName() {
    return projectName;
}

function getActiveFileId() {
    return activeFileId;
}

function getOpenedProjectFiles() {
    var keys = Object.keys(openedProjectFiles);
    var openedFilesId = [];
    keys.forEach(function (element, index) {
        openedFilesId.push(Number(element));
    });
    return openedFilesId;
}

function setActiveFileId(fileId) {

    if (fileId == activeFileId) return;

    activeFileId = fileId;

    var activeFileJson = {
        action: 'activeFileSet',
        projectId: getProjectId(),
        userId: getUserId(),
        fileId: fileId
    };

    realTimeSocket.send(JSON.stringify(activeFileJson));
}

function selectTab(tabDiv) {
    if (tabClosed != null) { return; }

    var id = Number($( tabDiv ).siblings( ".hidden-element-id" ).text());

    setActiveFileId(id);
    setFileContentInACEEditor(id);
}

function openFile(fileLi) {
    var id = Number($( fileLi ).children( ".hidden-element-id" ).text());

    setActiveFileId(id);

    if (openedProjectFiles[id] == undefined) {
        getFileContent(id);
    } else {
        setFileContentInACEEditor(id);
    }
}

function setFileContentInACEEditor(fileId) {

    if (editor == null) {
        initEditor();
    }

    var fileContent = openedProjectFiles[fileId].slice();

    editor.off("change", handleLocalChanges.onUserChangeAce);
    editor.setValue(fileContent, -1);
    editor.on("change", handleLocalChanges.onUserChangeAce);

    setCompilationErrors();
}

function handleRenderProjectOutline(data) {
    if (data.status == "success") {

        addErrorMarkersInProjectOutline();
        displayCollaboratorActiveFileInProjectStructure();

        if (filePendingToBeDisplayed != null) {
            
            var id = filePendingToBeDisplayed.id;
            var path = filePendingToBeDisplayed.path;
            var pathArray = path.split('/');
            pathArray.splice(0, 1);
            var rootChildren = $( "#project-outline" ).children( "ul.children" );

            clickNewlyCreatedFile(rootChildren, pathArray, 0, id);

            filePendingToBeDisplayed = null;
        }
    }
}

/**
 * Iterate through the DOM elements of the project outline following a DFS algorithm.
 * When the newly created file is found, trigger a click to update the server and
 * display the content in the IDE
 *
 * @param arrayElement
 * @param pathArray
 * @param index
 * @param fileId
 * @returns {*}
 */
function clickNewlyCreatedFile(arrayElement, pathArray, index, fileId) {
    if (index == pathArray.length - 1) {
        var files = arrayElement.children( "a" );
        files.each(function() {
            var id = Number(
                $(this).children( "li.file" ).children( ".hidden-element-id" ).text());
            if (id == fileId) {
                $(this).click(); // update server and refresh tabs
                $(this).children( "li.file" ).click(); // open file
            }
        });
    }

    var folders = arrayElement.children( "li.folder" );
    folders.each(function() {
        var folderName = $(this).children( ".folder-line" ).children( "p" ).text();
        if (folderName == pathArray[index]) {
            var children = $(this).children( "ul.children" );
            clickNewlyCreatedFile(children, pathArray, index + 1, fileId);
        }
    });
}

/**
 * When clicking an element on the project outline, the javascript code is executed first (will
 * display the file content in editor - may require requesting the content to node - and set the
 * file to be active - includes sending a request to node to notify other collaborators.
 *
 * Then the file selection is updated on the server (JSF) and the tab bar is refreshed (ajax).
 *
 * This method is called in response to the ajax request, after the tab bar has been refreshed
 * we need to update the collaborators markers - in case any of them are active on the newly opened
 * file.
 *
 * @param data
 */
function handleRenderTabBar(data) {
    if (data.status == "success") { // After the tab bar has been refreshed
        displayCollaboratorActiveFileInTabBar();
        addErrorMarkersInTabs();

        if (tabClosed != null) {
            handleTabWasClosed();
            tabClosed = null;
        }
    }
}

function closeTab(tabHtml) {
    var tab = $( tabHtml ).parent();
    tabClosed = Number(tab.siblings( '.hidden-element-id' ).text());
    tab.children( '.hidden-close-tab-button' ).click();
}

function preventPropagation(event) {
    event.stopPropagation();
}

function handleTabWasClosed() {

    if (tabClosed == activeFileId) {
        // Find the new active file, if any
        var selectedFileId = undefined;

        var openTabsHTML =  $( "#tab-bar" ).children( "a" );
        openTabsHTML.each(function() {
            var id = Number($(this).children( '.hidden-element-id' ).text());
            var tab = $(this).children( '.tab' );

            if (tab.attr('class').indexOf('selected') != -1) {
                selectedFileId = id;
            }
        });

        // Notify collaborators of new active file
        setActiveFileId(selectedFileId);

        if (selectedFileId == undefined) { // No more tabs opened, close editor
            editor.destroy();
            editor = null;
            $( '#ide-editor' ).empty();
        } else { // Set new selected file content in editor
            setFileContentInACEEditor(selectedFileId);
        }
    }

    // Remove the file content from memory
    delete openedProjectFiles[tabClosed];

    // Remove file from file connections in node
    var fileClosedJson = {
        action: 'fileClosed',
        projectId: getProjectId(),
        userId: getUserId(),
        fileId: tabClosed
    };

    realTimeSocket.send(JSON.stringify(fileClosedJson));
}

/**
 *These methods are called upon event onchange fired by Ace, it will be triggered on each:
 * - insert
 * - remove
 * - paste
 * events in the editor.
 *
 * @param data a json object in the form:
 *      {"start":{"row":0,"column":0},
 *      "end":{"row":0,"column":1},
 *      "action":"insert",
 *      "lines":["g"]}
 *
 *      or
 *
 *      {"start":{"row":2,"column":0},
 *      "end":{"row":2,"column":1},
 *      "action":"remove",
 *      "lines":["g"]}
 */
var handleLocalChanges = {

    /**
     *This method is called upon event onchange fired by Ace, it will be triggered on each:
     * - insert
     * - remove
     * - paste
     * events in the editor.
     *
     * This method convert the changes received by Ace to an ot.TextOperation, the operation is then
     * applied to the version of the document stored in openedProjectFiles before being applied to
     * the ot.Client corresponding to the active file (which will trigger sending the operation to the
     * server)
     *
     * @param data a json object in the form:
     *      {"start":{"row":0,"column":0},
     *      "end":{"row":0,"column":1},
     *      "action":"insert",
     *      "lines":["g"]}
     *
     *      or
     *
     *      {"start":{"row":2,"column":0},
     *      "end":{"row":2,"column":1},
     *      "action":"remove",
     *      "lines":["g"]}
     */
    onUserChangeAce: function(data) {
        var document = openedProjectFiles[getActiveFileId()];
        var documentArray = getContentAsArray(document);

        var startRow = data.start.row;
        var startColumn = data.start.column;
        var changes = joinContentArray(data.lines);

        var characterCountBefore = 0;
        var characterCountAfter = 0;

        for (var row in documentArray) {
            if (row < startRow) {
                characterCountBefore += documentArray[row].length + 1; // +1 for new line character
            } else if (row == startRow) {
                characterCountBefore += startColumn;
                characterCountAfter += documentArray[row].length - startColumn + 1;
            } else {
                characterCountAfter += documentArray[row].length + 1;
            }
        }

        characterCountAfter -= 1; // The last line doesnt contain a newLine character

        // Create the ot.TextOperation
        var operation;
        if (data.action == 'insert') {
            operation = new ot.TextOperation()
                .retain(characterCountBefore)
                .insert(changes)
                .retain(characterCountAfter);
        } else if (data.action == 'remove') {
            operation = new ot.TextOperation()
                .retain(characterCountBefore)
                .delete(changes)
                .retain(characterCountAfter - changes.length);
        } else {
            console.log('Unknown action: ' + data.action);
        }

        // Update the document
        openedProjectFiles[getActiveFileId()] = operation.apply(document);

        // Will trigger client.sendOperation
        fileClients[getActiveFileId()].applyClient(operation);
    },

    /**
     * Extract the changes from an ot.TextOperation and insert them into an editor.
     * Avoid loops by removing the listener before applying the operation and re-adding it after.
     *
     * Note that this implementation doesn't take into consideration composites operations, ie
     * [retain, insert, retain..., retain, delete, etc] but only one insert/delete with one
     * retain ops before and/or after
     *
     * @param operation
     */
    insertChangesIntoEditor: function(operation) {
        console.log("insert in editor: " + JSON.stringify(operation));

        var document = openedProjectFiles[getActiveFileId()];
        var documentArray = getContentAsArray(document);

        var startRow = 0;
        var startColumn = 0;
        var characterCountBefore = 0;

        for (var i in operation.ops) {
            var op = operation.ops[i];

            if (ot.TextOperation.isRetain(op)) {
                console.log("op: " + op + " is retain");

                // Find the row and the position to insert
                while (1) {
                    var lineCharacterCount = documentArray[startRow].length + 1; // +1 for newline char
                    if (characterCountBefore + lineCharacterCount > op) {
                        startColumn = op - characterCountBefore;
                        break;
                    } else {
                        characterCountBefore += lineCharacterCount;
                        startRow++;
                    }
                }

            } else if (ot.TextOperation.isInsert(op)) {
                console.log("op: " + op + " is insert");

                var position = {
                    row: startRow,
                    column: startColumn
                };

                editor.off("change", handleLocalChanges.onUserChangeAce);
                editor.session.insert(position, op);
                editor.on("change", handleLocalChanges.onUserChangeAce);

                break; // Ignore retain after insert

            } else if (ot.TextOperation.isDelete(op)) {
                console.log("op: " + op + " is delete");

                var endRow = startRow;
                var endColumn = startColumn;
                var numberCharsToDelete = Math.abs(op);

                while (1) {
                    var lineCharacterCount2 = documentArray[endRow].length + 1; // +1 for newline char
                    if (endColumn + numberCharsToDelete < lineCharacterCount2) {
                        endColumn += numberCharsToDelete;
                        break;
                    } else {
                        endRow++;
                        numberCharsToDelete -= (lineCharacterCount2 - endColumn);
                        endColumn = 0;
                    }
                }

                var range = new AceRange(startRow, startColumn, endRow, endColumn);

                editor.off("change", handleLocalChanges.onUserChangeAce);
                editor.session.remove(range);
                editor.on("change", handleLocalChanges.onUserChangeAce);

                break;

            } else {
                // Shouldn't get here
                console.log("Unknown op type, op: " + op);

            }
        }
    }
};

function getContentAsArray(content) {
    return content.split('\n');
}

function joinContentArray(contentArray) {
    return contentArray.join('\n');
}
