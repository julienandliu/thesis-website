Setting Up
========================

## Author(s)
*   Romain Julien
*   Lisa Liu-Thorrold

--------------------------------------------------------------------------------
## Getting started
##### These instructions are for setup using the Intellij IDE 
1. Clone the repository from the following location

    https://lisa_liu-thorrold@bitbucket.org/julienandliu/thesis-website.git
    
2. Import the project you have cloned into Intellij as a normal/regular project

3. Now we need to set up the artifacts for deployment on JBoss. Make sure you have JBoss downloaded from the following URL

    http://www.jboss.org/products/eap/download/
    
    Once downloaded, go to IntelliJ IDEA -> Preferences -> Build,Execution,Deployment -> Application Servers. Click the + sign, and add a JBoss Application Server. Set it's home to be where you downloaded JBoss.
    
4. Now we need to set up the project structure appropriately. Go to File -> Project Structure -> Artifacts. Here we are going to create 3 artifacts, we'll start with .ejb. Click on the + and select "EJB Application: Exploded". Select "From Modules...", and click ok (thesis-website should be selected). The Artifact should have 4 items: 
    1. 'thesis-website' compile output folder
    2. 'thesis-website' module: 'EJB' facet resources
    3. 'thesis-website' module: 'javaEEApplication' facet resources
    4. 'thesis-website' module: 'Web' facet resources
    5. Now make sure the Output directory has a .ejb at the end of it. If it doesn't then add it on.
    
5. Now create another Artifact, this time select "Web Application: Exploded". It should have the following 4 items automatically, once you select thesis-website from modules like above:
    1. WEB-INF folder
    2. 'thesis-website' module: 'EJB' facet resources
    3. 'thesis-website' module: 'javaEEApplication' facet resources
    4. 'thesis-website' module: 'Web' facet sources
    5. Also make sure the Output directory has a .war at the end of it. If it doesn't then add it on.
    
6. Finally, we'll create a "JavaEE Application: Exploded" Artifact. You will have to populate this one manually from within.
    1. Create a folder called 'ejb.jar' and add the thesis-website:ejb exploded artifact to it
    2. Create a folder called 'web.war' and add the thesis-website:war exploded artifact to it
    3. Add all the libraries in the project
    4. Add a JaveEE Facet Resource (should be called 'thesis-website' module: 'javaEEApplicatin' facet resources.
    5. Make sure the Output directory has a .ear at the end of it. If it doesn't then add it on.
    
7. Next step, we should be able to build the Artifacts. To do this, go to Build -> Build Artifacts...

8. Now we'll make a run configuration so that we can deploy our application with a single click. Go to Run -> Edit Configurations
9. Add a new 'JBoss Server' configuration, and call it JBoss
10. Now make sure a browser is selected after launch (Google Chrome or Firefox works best) and have the URL go to the following:       

http://localhost:8080/thesis-javaeeWeb/pages/index.xhtml

11. Click the Deployment tab, and add the 'thesis-website:ear exploded' artifact.

12. Now go back to the Server tab, and add a 'Build Artifacts' and select all 3 artifacts under the Before Launch section.

13. Before we run the application, we need to make sure MongoDB is running. Download mongodb if you haven't already yet, from 

https://www.mongodb.com/download-center#community

14. cd into the /bin folder of the unzipped location of MongoDB, and run ./mongod. You may get a prompt saying you don't have a folder, or you don't have permissions to the following folder: /data/db
    If you do, cd back into the main folder and run the following command:

    sudo mkdir -p /data/db
    sudo chown -R $(whoami) /data/db
    
    And try starting MongoDB again
    
15. In src/main/java/SystemManagement/ProjectManagementBean
    change PATH_PROJECTS_FOLDERS to be the path where you want the mongo db records to be
    
    eg. private final String PATH_PROJECTS_FOLDERS = "/Users/Lisa/Desktop/mongo/";


16.  You can now run the application. Go to Run -> Run 'JBoss'.

IMPORTANT: Make sure the war/ear does not bundles Weld Servlet artifacts/libraries. Weld Servlet is only intended for plain servlet containers (Tomcat, Jetty, etc.) and not for WildFly/JBoss (or any other Java EE application server) where the CDI integration is available out of the box.