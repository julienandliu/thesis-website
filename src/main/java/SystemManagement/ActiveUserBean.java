package SystemManagement;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import SystemManagement.Entity.UserDocument;

/**
 * Created by romainjulien on 22/07/2016.
 */
@ManagedBean(name = "ActiveUser", eager = true)
@SessionScoped
public class ActiveUserBean {

    private UserDocument activeUser;

    public ActiveUserBean() {}

    public UserDocument getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(UserDocument activeUser) {
        this.activeUser = activeUser;
    }

    public String signOut() {
        activeUser = null;
        return "/pages/index?faces-redirect=true";
    }
}
