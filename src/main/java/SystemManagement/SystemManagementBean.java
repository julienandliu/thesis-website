package SystemManagement;

import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import MongoConfig.MongoConnection;
import SystemManagement.Entity.ProjectDocument;
import SystemManagement.Entity.UserDocument;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created by romainjulien on 21/04/2016.
 */
@ManagedBean(name = "SystemManagement", eager = true)
@ViewScoped
public class SystemManagementBean {

    @ManagedProperty("#{ActiveUser.activeUser}")
    private UserDocument activeUser;

    private List<ProjectDocument> myProjectList;

    private List<ProjectDocument> sharedProjectList;

    public SystemManagementBean() {

    }

    public UserDocument getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(UserDocument activeUser) {
        this.activeUser = activeUser;
    }

    public List<ProjectDocument> getMyProjectList() {
        Query myProjectsQuery = new Query(where("ownerId").is(activeUser.getId()));
        myProjectList = MongoConnection.getInstance().getmongoOperations().find(myProjectsQuery, ProjectDocument.class);
        return myProjectList;
    }

    public void setMyProjectList(List<ProjectDocument> myProjectList) {
        this.myProjectList = myProjectList;
    }

    public List<ProjectDocument> getSharedProjectList() {
        Query myProjectsQuery = new Query(where("collaborators").in(activeUser.getId()));
        sharedProjectList = MongoConnection.getInstance().getmongoOperations().find(myProjectsQuery, ProjectDocument.class);
        return sharedProjectList;
    }

    public void setSharedProjectList(List<ProjectDocument> sharedProjectList) {
        this.sharedProjectList = sharedProjectList;
    }

}
