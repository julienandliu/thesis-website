package SystemManagement;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import MongoConfig.MongoConnection;
import SystemManagement.Entity.CounterIdDocument;
import SystemManagement.Entity.UserDocument;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created by romainjulien on 9/07/2016.
 */
@ManagedBean(name = "AccountManagement", eager = true)
@ViewScoped
public class AccountManagementBean {

    private UserDocument loggedInUser;

    private String loginEmail;
    private String loginPassword;

    private String email;
    private String password;
    private String firstName;
    private String lastName;

    private String loginUnsuccessful = null;
    private String signupUnsuccessful = null;

    public AccountManagementBean() {
    }

    public UserDocument getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(UserDocument loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginUnsuccessful() {
        return loginUnsuccessful;
    }

    public void setLoginUnsuccessful(String loginUnsuccessful) {
        this.loginUnsuccessful = loginUnsuccessful;
    }

    public String getSignupUnsuccessful() {
        return signupUnsuccessful;
    }

    public void setSignupUnsuccessful(String signupUnsuccessful) {
        this.signupUnsuccessful = signupUnsuccessful;
    }

    public String login() {
        UserDocument loggedInUser = MongoConnection.getInstance().getmongoOperations()
                .findOne(new Query(where("email").is(loginEmail)), UserDocument.class);
        if (loggedInUser != null) {
            if (loggedInUser.getPassword().equals(loginPassword)) {
                // Login successful
                setActiveUser(loggedInUser);
                loginUnsuccessful = null;
                return "home/home?faces-redirect=true";
            } else {
                loginUnsuccessful = "Incorrect password";
                return null;
                // handle user exists but password incorrect
            }
        } else {
            loginUnsuccessful = "User does not exist";
            return null;
            // handle user doesnt exists
        }
    }

    public String signUp() {
        Query userQuery = new Query(where("email").is(email));
        UserDocument newUser = MongoConnection.getInstance().getmongoOperations().findOne(userQuery, UserDocument.class);
        if (newUser != null) {
            // handle user already exist
            signupUnsuccessful = "User already exists";
            return null;
        }

        int id = getNextId();
        newUser = new UserDocument(id, firstName, lastName, email, password);
        MongoConnection.getInstance().getmongoOperations().save(newUser);

        setActiveUser(newUser);
        signupUnsuccessful = null;
        return "home/home?faces-redirect=true";
    }

    private int getNextId() {
        Query query = new Query(where("sequence").is("user"));
        Update update = new Update().inc("counter", 1);

        CounterIdDocument counter = MongoConnection.getInstance().getmongoOperations().findAndModify(query, update,
                new FindAndModifyOptions().returnNew(true), CounterIdDocument.class);

        if (counter != null) {
            return counter.getCounter();
        } else {
            CounterIdDocument userCounter = new CounterIdDocument("user");
            MongoConnection.getInstance().getmongoOperations().save(userCounter);
            return userCounter.getCounter();
        }
    }

    private void setActiveUser(UserDocument user) {
        FacesContext context = FacesContext.getCurrentInstance();
        ActiveUserBean auBean = context.getApplication().evaluateExpressionGet(
                context, "#{ActiveUser}", ActiveUserBean.class);
        auBean.setActiveUser(user);
    }
}
