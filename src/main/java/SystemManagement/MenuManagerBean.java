package SystemManagement;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Created by romainjulien on 22/07/2016.
 */
@ManagedBean(name = "MenuManager", eager = true)
@SessionScoped
public class MenuManagerBean {

    private String menuSelected = "my-projects";

    public MenuManagerBean() {}

    public boolean isMenuSelected(String menu) {
        return menuSelected.equals(menu);
    }

    public String getMenuSelected() { return menuSelected; }

    public void setMenuSelected(String menuSelected) {
        this.menuSelected = menuSelected;
    }

    public String getHeaderTitle() {
        switch (menuSelected) {
            case "my-projects":
                return "My Projects";
            case "shared-projects":
                return "Shared projects";
            case "trash":
                return "Trash";
            default:
                break;
        }
        return null;
    }

    public String getIncludePage() {
        switch (menuSelected) {
            case "my-projects":
                return "my-projects-content-page.xhtml";
            case "shared-projects":
                return "my-projects-content-page.xhtml";
            case "trash":
                return "trash-content-page.xhtml";
            default:
                break;
        }
        return null;
    }

    public void resetMenuSelected() {
        menuSelected = "my-projects";
    }

}
