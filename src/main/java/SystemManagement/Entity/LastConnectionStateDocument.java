package SystemManagement.Entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

import Ide.ProjectElement;

/**
 * Created by romainjulien on 15/08/2016.
 */
@Document(collection = "lastConnectionState")
public class LastConnectionStateDocument {

    @Id
    private ObjectId id;
    private int projectId;
    private int userId;
    private ProjectElement lastActiveFile;
    private ArrayList<ProjectElement> lastOpenTabs;

    public LastConnectionStateDocument(ObjectId id, int projectId, int userId, ProjectElement lastActiveFile, ArrayList<ProjectElement> lastOpenTabs) {
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.lastActiveFile = lastActiveFile;
        this.lastOpenTabs = lastOpenTabs;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ProjectElement getLastActiveFile() {
        return lastActiveFile;
    }

    public void setLastActiveFile(ProjectElement lastActiveFile) {
        this.lastActiveFile = lastActiveFile;
    }

    public ArrayList<ProjectElement> getLastOpenTabs() {
        return lastOpenTabs;
    }

    public void setLastOpenTabs(ArrayList<ProjectElement> lastOpenTabs) {
        this.lastOpenTabs = lastOpenTabs;
    }
}
