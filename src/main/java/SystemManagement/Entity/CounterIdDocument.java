package SystemManagement.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by romainjulien on 18/07/2016.
 */
@Document(collection = "counterId")
public class CounterIdDocument {

    @Id
    private String sequence;
    private int counter;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public CounterIdDocument(String sequence) {
        this.sequence = sequence;
        counter = 1;
    }

    @Override
    public String toString() {
        return ("Sequence: " + sequence + " Counter: " + counter);
    }
}
