package SystemManagement.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Ide.ProjectElement;

/**
 * Created by romainjulien on 7/05/2016.
 */
@Document (collection = "projects")
public class ProjectDocument {

    @Id
    private int id;
    private String projectName;
    private int ownerId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;
    private String pathToProjectFolder;
    private List<Integer> collaborators;
    private List<SnapshotDocument> snapshots;
    private ArrayList<ProjectElement> projectStructure;

    public ProjectDocument() {
    }

    public ProjectDocument(int id, String projectName, int ownerId, Date createdDate,
                           String pathToProjectFolder, List<Integer> collaborators,
                           List<SnapshotDocument> snapshots, ArrayList<ProjectElement>  projectStructure) {
        this.id = id;
        this.projectName = projectName;
        this.ownerId = ownerId;
        this.createdDate = createdDate;
        this.pathToProjectFolder = pathToProjectFolder;
        this.collaborators = collaborators;
        this.snapshots = snapshots;
        this.projectStructure = projectStructure;
    }

//    public ProjectDocument(int id, String projectName, int ownerId, Date createdDate, String pathToProjectFolder) {
//        this.id = id;
//        this.projectName = projectName;
//        this.ownerId = ownerId;
//        this.createdDate =createdDate;
//        this.pathToProjectFolder = pathToProjectFolder;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getPathToProjectFolder() {
        return pathToProjectFolder;
    }

    public void setPathToProjectFolder(String pathToProjectFolder) {
        this.pathToProjectFolder = pathToProjectFolder;
    }

    public List<Integer> getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(List<Integer> collaborators) {
        this.collaborators = collaborators;
    }

    public List<SnapshotDocument> getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(List<SnapshotDocument> snapshots) {
        this.snapshots = snapshots;
    }

    public ArrayList<ProjectElement>  getProjectStructure() {
        return projectStructure;
    }

    public void setProjectStructure(ArrayList<ProjectElement>  projectStructure) {
        this.projectStructure = projectStructure;
    }

    @Override
    public String toString() {
        return ("Id: " + id + "Name: " + projectName + ", owner: " + ownerId);
    }
}
