package SystemManagement.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by romainjulien on 10/05/2016.
 */
@Document(collection = "snapshots")
public class SnapshotDocument {

    @Id
    private String id;
    private String projectId;
    private String name;

    public SnapshotDocument(String id, String projectId, String name) {
        this.id = id;
        this.projectId = projectId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
