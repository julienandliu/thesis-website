package SystemManagement;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import MongoConfig.MongoConnection;
import SystemManagement.Entity.ProjectDocument;
import SystemManagement.Entity.SnapshotDocument;
import SystemManagement.Entity.UserDocument;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created by romainjulien on 21/04/2016.
 */
@ManagedBean(name = "ProjectManagement", eager = true)
@SessionScoped
public class ProjectManagementBean {

    @ManagedProperty(value = "#{ActiveUser}")
    private ActiveUserBean activeUserBean;

    private ProjectDocument projectSelected = null;

    private List<UserDocument> collaboratorsList;
    private List<SnapshotDocument> snapshotsList;

    private String newCollaborator;

    private String errorMessageCollaborator = null;

    public ProjectManagementBean() {

    }

    public ProjectDocument getProjectSelected() { return projectSelected; }

    public void setProjectSelected(ProjectDocument projectSelected) {
        this.projectSelected = projectSelected;
    }

    public ActiveUserBean getActiveUserBean() {
        return activeUserBean;
    }

    public void setActiveUserBean(ActiveUserBean activeUserBean) {
        this.activeUserBean = activeUserBean;
    }

    public List<UserDocument> getCollaboratorsList() {
        if (projectSelected == null) return null;

        List<Integer> projectCollaborators = projectSelected.getCollaborators();

        if (projectCollaborators == null) return null;

        if (projectCollaborators.contains(activeUserBean.getActiveUser().getId())) { // the user is not the project owner
            projectCollaborators.remove(new Integer(activeUserBean.getActiveUser().getId()));
            projectCollaborators.add(0, projectSelected.getOwnerId());
        }

        List<UserDocument> collaborators = new ArrayList<>();

        for (Integer userId : projectCollaborators) {
            UserDocument collaborator = MongoConnection.getInstance().getmongoOperations().findById(userId, UserDocument.class);
            collaborators.add(collaborator);
        }

        return collaborators;
    }

    public void setCollaboratorsList(List<UserDocument> collaboratorsList) {
        this.collaboratorsList = collaboratorsList;
    }

    public List<SnapshotDocument> getSnapshotsList() {
        if (projectSelected == null) { return null; }
        return projectSelected.getSnapshots();
    }

    public void setSnapshotsList(List<SnapshotDocument> snapshotsList) {
        this.snapshotsList = snapshotsList;
    }

    public String getErrorMessageCollaborator() {
        return errorMessageCollaborator;
    }

    public void setErrorMessageCollaborator(String errorMessageCollaborator) {
        this.errorMessageCollaborator = errorMessageCollaborator;
    }

    public String getNewCollaborator() {
        return newCollaborator;
    }

    public void setNewCollaborator(String newCollaborator) {
        this.newCollaborator = newCollaborator;
    }

    public void addNewCollaborator() {

        // Project structure may have been updated, refresh project
        Query projectQuery = new Query(where("id").is(projectSelected.getId()));
        projectSelected = MongoConnection.getInstance().getmongoOperations().findOne(projectQuery, ProjectDocument.class);

        Integer userId;
        UserDocument user = MongoConnection.getInstance().getmongoOperations().findOne(new Query(where("email").is(newCollaborator)),
                UserDocument.class);
        if (user != null) {
            userId = user.getId();
        } else {
            errorMessageCollaborator = "User doesn't exist";
            return;
        }

        List<Integer> current = projectSelected.getCollaborators();
        if (current == null) {
            current = new ArrayList<>();
        } else if (current.contains(userId)) {
            errorMessageCollaborator = "This user already collaborates on the project";
            return;
        }

        current.add(userId);

        errorMessageCollaborator = null;

        // update projectselected and save new collaborator
        Query query = new Query(where("id").is(projectSelected.getId()));
        Update update = new Update().set("collaborators", current);
        projectSelected = MongoConnection.getInstance().getmongoOperations().findAndModify(query, update,
                new FindAndModifyOptions().returnNew(true), ProjectDocument.class);
    }
}
