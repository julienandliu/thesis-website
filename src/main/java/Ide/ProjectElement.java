package Ide;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;

/**
 * Created by romainjulien on 13/08/2016.
 */
public class ProjectElement {

    @Id
    private int id;
    private String name;
    private String type;
    private String path;
    private String packageName;
    private ArrayList<ProjectElement> children;

    public ProjectElement(int id, String name, String type, String path, String packageName, ArrayList<ProjectElement> children) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.path = path;
        this.packageName = packageName;
        this.children = children;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ArrayList<ProjectElement> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<ProjectElement> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object other){
        if (!(other instanceof ProjectElement))return false;
        ProjectElement otherPE =  (ProjectElement) other;
        return this.getId() == otherPE.getId();
    }

}
