package Ide;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import MongoConfig.MongoConnection;
import SystemManagement.Entity.LastConnectionStateDocument;
import SystemManagement.Entity.ProjectDocument;
import SystemManagement.Entity.UserDocument;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * Created by romainjulien on 11/05/2016.
 */
@ManagedBean(name = "IdeManagement", eager = true)
@ViewScoped
public class IdeManagementBean {

    @ManagedProperty("#{ProjectManagement.projectSelected}")
    private ProjectDocument projectSelected;

    @ManagedProperty("#{ActiveUser.activeUser}")
    private UserDocument activeUser;

    private String projectPath = null;
    private List<ProjectElement> openTabs = null;
    private ProjectElement fileSelected = null;

    private ArrayList<ProjectElement> projectElements;

    public IdeManagementBean() {

    }

    public ProjectDocument getProjectSelected() {
        return projectSelected;
    }

    public void setProjectSelected(ProjectDocument projectSelected) {
        this.projectSelected = projectSelected;
    }

    public UserDocument getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(UserDocument activeUser) {
        this.activeUser = activeUser;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String path) {
        this.projectPath = path;
    }

    public List<ProjectElement> getOpenTabs() {
        if (openTabs == null) { // when the IDE is first loaded
            openTabs = new ArrayList<>();

            Query lastConnectionStateQuery = new Query(
                    where("projectId").is(projectSelected.getId())
                            .and("userId").is(activeUser.getId()));

            LastConnectionStateDocument lastConnectionState = MongoConnection.getInstance().getmongoOperations().findOne(
                    lastConnectionStateQuery, LastConnectionStateDocument.class);

            if (lastConnectionState == null) {
                return openTabs;
            }

            fileSelected = lastConnectionState.getLastActiveFile();

            for (ProjectElement element : lastConnectionState.getLastOpenTabs()) {
                openTabs.add(element);
            }
        }

        return openTabs;
    }

    public void setOpenTabs(List<ProjectElement> openTabs) {
        this.openTabs = openTabs;
    }

    public ProjectElement getFileSelected() {
        return fileSelected;
    }

    public void setFileSelected(ProjectElement fileSelected) {
        if (!openTabs.contains(fileSelected)) {
            openTabs.add(fileSelected);
        }
        this.fileSelected = fileSelected;
    }

    public void closeTab(ProjectElement fileClosed) {
        if (openTabs.size() == 1) {
            openTabs.remove(fileClosed);
            fileSelected = null;
            return;
        }

        if (fileClosed.getId() == fileSelected.getId()) {
            int index = 0;
            for (ProjectElement element : openTabs) {
                if (element.getId() == fileClosed.getId()) {
                    break;
                } else {
                    index++;
                }
            }

            if (index == 0) {
                fileSelected = openTabs.get(index + 1);
            } else {
                fileSelected = openTabs.get(index - 1);
            }
        }

        openTabs.remove(fileClosed);
    }

    public ArrayList<ProjectElement> getProjectElement() {
        projectElements = new ArrayList<>();

        // Project structure has been updated, refresh project
        Query projectQuery = new Query(where("id").is(projectSelected.getId()));
        projectSelected = MongoConnection.getInstance().getmongoOperations().findOne(projectQuery, ProjectDocument.class);

        setProjectElementDFS(projectSelected.getProjectStructure());

        return projectElements;
    }

    private void setProjectElementDFS(ArrayList<?> arrayElement) {

        for (Object elementObj : arrayElement) {

            ProjectElement element = (ProjectElement) elementObj;

            projectElements.add(element);

            if (element.getType().equals("folder") || element.getType().equals("package")) {

                if (element.getChildren().size() != 0) {

                    // Pass the children as argument so we can calculate the size of the ul in ide.xhtml
                    projectElements.add(new ProjectElement(-1, null, "openChildren", null, null, element.getChildren()));
                    setProjectElementDFS(element.getChildren());
                    projectElements.add(new ProjectElement(-1, null, "closeChildren", null, null, null));
                }

                projectElements.add(new ProjectElement(-1, null, "closeFolder", null, null, null));
            }
        }
    }

    public void saveLastConnectionState() {
        Query lastConnectionStateQuery = new Query(
                where("projectId").is(projectSelected.getId())
                        .and("userId").is(activeUser.getId()));

        Update update = new Update().set("lastActiveFile", fileSelected)
                .set("lastOpenTabs", openTabs);

        MongoConnection.getInstance().getmongoOperations().findAndModify(lastConnectionStateQuery, update,
                new FindAndModifyOptions().upsert(true), LastConnectionStateDocument.class);
    }
}
