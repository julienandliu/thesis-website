package MongoConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by romainjulien on 11/09/2016.
 */
public class MongoConnection {
    private MongoTemplate mongoTemplate;

    private static MongoConnection ourInstance = new MongoConnection();

    public static MongoConnection getInstance() {
        return ourInstance;
    }

    private MongoConnection() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
        mongoTemplate = (MongoTemplate) ctx.getBean("mongoTemplate");
    }

    public MongoTemplate getmongoOperations() {
        return mongoTemplate;
    }

    public void closeConnection() {
        mongoTemplate.getDb().getMongo().close();
    }
}
